import java.util.*;

public class RoyalRumble {

  // getSortedList method uses custom comparator RoyalNameComparator within same directory
  public static ArrayList<String> getSortedList(ArrayList<String> names) {
    ArrayList<String> sortedNames = names;
    Collections.sort(sortedNames, new RoyalNameComparator());
    return sortedNames;
  }

  // main method to test output of getSortedList method
  public static void main(String[] args) {
    ArrayList<String> testList = new ArrayList<>();
    testList.add("Louis IX");
    testList.add("Louis VIII");
    testList.add("David II");
    ArrayList<String> sortedList = getSortedList(testList);
    for(String name : sortedList) {
      System.out.println(name);
    }
  }
}
