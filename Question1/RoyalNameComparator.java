import java.util.*;

public class RoyalNameComparator implements Comparator<String> {
  // private attribute to store map of key -> roman character, and value -> integer value of character
  private Map<Character, Integer> romanCharMap;

  // constructor
  public RoyalNameComparator() {
    super();
    romanCharMap = new HashMap<>();
    romanCharMap.put('I', 1);
		romanCharMap.put('V', 5);
		romanCharMap.put('X', 10);
		romanCharMap.put('L', 50);
  }

  // Comparator's compare method
  public int compare(String name1, String name2) {
    String[] name1Arr = name1.split(" ");
		String[] name2Arr = name2.split(" ");

    if (name1Arr[0].compareTo(name2Arr[0]) == 0) {
			int roman1 = convertRomanToInt(name1Arr[1]);
			int roman2 = convertRomanToInt(name2Arr[1]);

      if(roman1 == roman2) {
        return 0;
      } else if(roman1 > roman2) {
        return 1;
      } else {
        return -1;
      }
		} else {
			return name1Arr[0].compareTo(name2Arr[0]);
		}
  }

  // private method to convert roman numerals to integer value
  private int convertRomanToInt(String roman) {
    char[] arr = roman.toCharArray();

    int max = 0;
		int total = 0;
    // iterate in reverse to correctly determine roman numeral value
    // eg. IX should return 9 instead of 10
		for (int i = arr.length - 1; i >= 0; i--) {
			int val = romanCharMap.get(arr[i]);
			if (val >= max) {
				max = val;
				total += val;
			} else {
				total -= val;
			}
		}
		return total;
  }
}
