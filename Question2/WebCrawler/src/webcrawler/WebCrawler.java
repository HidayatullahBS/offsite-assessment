/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webcrawler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 *
 * @author hidayat
 */
public class WebCrawler {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            App();
        } catch(InputMismatchException e) {
            System.out.println("Wrong input type! Please type an integer value");
        } catch(IOException e) {
            System.out.println("Oops! Error occurred while writing to file");
        }
    }
    
    private static void App() throws IOException {
        Scanner sc = new Scanner(System.in);
        // Retrieve console input to determine language
        int languageValue = 0;
        String languageUrl = "";
        do {
            System.out.println("\nLanguages:\n"
                + "1. All languages\n"
                + "2. Unknown languages\n"
                + "3. C++\n"
                + "4. HTML\n"
                + "5. Java\n"
                + "6. JavaScript\n"
                + "7. PHP\n"
                + "8. Python\n"
                + "9. Ruby");
            System.out.print("Select a value between 1 and 9: ");
            languageValue = sc.nextInt();
            sc.nextLine();
            
            switch(languageValue) {
                case 1:
                    languageUrl = "";
                    break;
                case 2:
                    languageUrl = "/unknown";
                    break;
                case 3:
                    languageUrl = "/c++";
                    break;
                case 4:
                    languageUrl = "/html";
                    break;
                case 5:
                    languageUrl = "/java";
                    break;
                case 6:
                    languageUrl = "/javascript";
                    break;
                case 7:
                    languageUrl = "/php";
                    break;
                case 8:
                    languageUrl = "/python";
                    break;
                case 9:
                    languageUrl = "/ruby";
                    break;
                default:
                    System.out.println("Please select a value between 1 and 9");
                    break;
            }
        } while(languageValue < 1 || languageValue > 9);
        
        
        int time = 0;
        String timespanUrl = "";
        do {
            // Retrive console input to determine time span
            System.out.println("\nTime span:\n"
                + "1. Today\n"
                + "2. This week\n"
                + "3. This month");
            System.out.print("Select a value between 1 and 3: ");
            time = sc.nextInt();
            
            switch(time) {
                case 1:
                    timespanUrl = "?since=daily";
                    break;
                case 2:
                    timespanUrl = "?since=weekly";
                    break;
                case 3:
                    timespanUrl = "?since=monthly";
                    break;
                default:
                    System.out.println("Please select a value between 1 and 3");
                    break;
            }
        } while(time < 1 || time > 3);
        
        
        // parse html content and get document object
        Document doc = Jsoup.connect("https://github.com/trending" + languageUrl + timespanUrl).get();
        Element result = doc.select(".repo-list").first();
        
        // Stores k = rank of item, v = array of values to print to csv
        Map<Integer, ArrayList<String>> resultsMap = new HashMap<>();
        
        Elements nameElements = result.getElementsByTag("h3");
        
        int rank = 1;
        
        // for each element, retrieve the name of the repo and store into map value as array
        for(Element name : nameElements) {
            ArrayList<String> values = new ArrayList<>();
            values.add(name.text().replace(" ",""));
            resultsMap.put(rank, values);
            rank++;
        }
        
        // Retrieve parent elements required for number of stars and programming language
        Elements parentElements = result.getElementsByClass("f6 text-gray mt-2");
        
        // Reset rank to determine mapping of values
        rank = 1;
        
        for(Element childNode : parentElements) {
            /*** Retrieving number of stars ***/
            Element starsElement = childNode.getElementsByClass("muted-link d-inline-block mr-3").first();
            ArrayList<String> values = resultsMap.get(rank);
            values.add(starsElement.text().replace(",", ""));
            
            /*** Retrieving programming languages ***/
            // get number of child elements
            int nodeSize = childNode.childNodeSize();
            
            // If child node size is 11, assign the programming language,
            // else, assign "Unknown language"
            // Elements childElements = e.children();
            if(nodeSize == 11) {
                Element languageElement = childNode.getElementsByAttribute("itemprop").first();
                values.add(languageElement.text());
                resultsMap.put(rank, values);
            } else {
                values.add("Unknown language");
                resultsMap.put(rank, values);
            }
            rank++;
        }
        
        /*** Write to CSV file ***/
        File file = new File("Trending.csv");
        FileOutputStream out = new FileOutputStream(file, false);
        
        // For headers
        String headers = "Github repo name,Number of stars,Programming language\n";
        out.write(headers.getBytes());
        
        rank = 1;
        while(rank <= resultsMap.size()) {
            ArrayList<String> resultArr = resultsMap.get(rank);
            for(int i = 0; i < resultArr.size(); i++) {
                String toPrint = resultArr.get(i);
                if(i != resultArr.size() - 1) {
                    toPrint = toPrint.concat(",");
                } else {
                    toPrint = toPrint.concat("\n");
                }
                out.write(toPrint.getBytes());
            }
            rank++;
        }
        
        out.close();
    }
}
